﻿using System; 
using Calculations;
using static System.Console; 
using System.Collections.Generic;


namespace BMI_Assignment_GT
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var c1 = new Calc1();
            Write("\n Hello! It's time to calculate your Body Mass Index (BMI).\n\n Please enter your weight in pounds.\t");
            int weight = Convert.ToInt32(ReadLine());
            //  WriteLine(c1.Testing123()); how to call methods from the library

            Write("\n Please enter your height in inches.\t");

            int height = Convert.ToInt32(ReadLine());



            string bmi = Convert.ToString(c1.BMI_Calc(weight, height));

            int d_bmi = Convert.ToInt32(bmi);

            if (d_bmi <= 17.5)
            {
                WriteLine($" \n Your BMI is {d_bmi} and is classified as Anorexic. ");

            }

            if (d_bmi > 17.5 && d_bmi < 18.5)
            {
                WriteLine($" \n Your BMI is {d_bmi} and is classified as Underweight. ");

            }

            if (d_bmi >= 18.5 && d_bmi < 25)
            {
                WriteLine($" \n Your BMI is {d_bmi} and is classified as Ideal. ");

            }

            if (d_bmi >= 25 && d_bmi < 30)
            {
                WriteLine($" \n Your BMI is {d_bmi} and is classified as Overweight. ");

            }

            if (d_bmi >= 30 && d_bmi < 40)
            {
                WriteLine($" \n Your BMI is {d_bmi} and is classified as Obese. ");

            }

            if (d_bmi > 40)
            {
                WriteLine($" \n Your BMI is {d_bmi} and is classified as Morbidly Obese. ");

            }

            ReadLine(); // Pause    

        }

        /* ]Starvation: less than 15
 Anorexic: less than 17.5
 Underweight: less than 18.5
 Ideal: greater than or equal to 18.5 but less than 25
 Overweight: greater than or equal to 25 but less than 30
 Obese: greater than or equal to 30 but less than 40
 Morbidly Obese: greater than or equal to 40
         */
    }
}


